var timerWeightPerHour = 0.0;
var accumulatedCost = 0.0;
var stepMs = 100;
var intervalCounterId = 0;
var intervalCounter = function () {
  let splitter = 3600 * (1000 / stepMs);
  let incrementSize = timerWeightPerHour / splitter;
  accumulatedCost = accumulatedCost + incrementSize;
  let timerCounter = $('#timerCounter');
  timerCounter.text('$' + accumulatedCost.toFixed(2));
}

function startTimer() {
  let startBtn = $('#startTimer');
  startBtn.css('display', 'none');
  let stopBtn = $('#stopTimer');
  stopBtn.css('display', 'block');
  intervalCounterId = setInterval(intervalCounter, stepMs);
}

function stopTimer() {
  let startBtn = $('#startTimer');
  startBtn.css('display', 'block');
  let stopBtn = $('#stopTimer');
  stopBtn.css('display', 'none');
  clearInterval(intervalCounterId);
}

function resetTimer() {
  let timerCounter = $('#timerCounter');
  timerCounter.text('$0.00');
  accumulatedCost = 0.0;
}

function valueUpdate() {
  let costInput = $('#costPerHour');
  let parsedInput = Number(costInput.val());
  if (!isNaN(parsedInput)) {
    timerWeightPerHour = parsedInput;
  }
  else {
    console.log('Value "' + costInput.val() + '" is not a number');
  }
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    let vars = [], hash;
    let hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(let i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$( document ).ready(function () {
  let meetingCost = getUrlVars()['meetingCost'];
  let isValidCost = false;
  if (null !== meetingCost && undefined !== meetingCost) {
    let parsedInput = Number(meetingCost);
    if (!isNaN(parsedInput)) {
      timerWeightPerHour = parsedInput;
      let costInput = $('#costPerHour');
      costInput.val(parsedInput);
      isValidCost = true;
    }
    else {
      console.log('Value "' + meetingCost + '" is not a number');
    }
  }

  let startTimerParam = getUrlVars()['startTimer'];
  if (true === isValidCost && null !== startTimerParam && undefined !== startTimerParam) {
    startTimer();
  }
});
